from setuptools import setup, find_packages

with open("README.md", "r") as fd:
    long_description = fd.read()

setup(
    name="webdav-upload",
    version="0.0.1",
    description="Bulk WebDAV uploader",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/tools/webdav-upload",
    license="MIT License",
    classifiers=[
        "Environment :: Console",
        "Programming Language :: Python :: 3",
        "License :: OSI APproved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.5",
    install_requires=["webdavclient3"],
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "webdav-upload = webdav_upload.upload:main",
        ],
    }
)

