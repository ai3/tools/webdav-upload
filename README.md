webdav-upload
===

Upload the contents of a directory (recursively) to a remote WebDAV
server. Requires Python 3, and uses the fine
[webdavclient3](https://github.com/ezhov-evgeny/webdav-client-python-3)
library underneath.

# Installation

This should work:

```shell
$ pip3 install git+https://git.autistici.org/ai3/tools/webdav-upload#egg=webdav_upload
```

If all goes well you should end up with a *webdav-upload* executable
somewhere.

# Usage

To upload the contents of the local directory *dir/* to a remote
server, say *https://dav.example.com/~myuser*, with authentication,
you would run something like the following:

```shell
$ webdav-upload --user myuser --password pass --url https://dav.example.com/~myuser/ dir www
```

This would copy the contents of *dir* into the *www* directory of the
*/~myuser/* remote WebDAV root.

If you run the command a second time, the files will not be transfered
again, as the tool checks for their existence before uploading
them. Should you want to avoid this check (see "Limitations" below for
why it might make sense to always force an upload), add the *--force*
command-line option.

## Limitations

It's not a full synchronization solution, as it won't remove remote
files that no longer exist locally. Also, while it can perform
incremental transfers, it will not check whether the parameters
(timestamp, size) or contents of the local and remote files match.
